# Type Assertions in TypeScript

Type assertions in TypeScript are a way to tell the compiler that you know more about the type of a value than it does.

![](https://media.tenor.com/Vp7wA5EJBygAAAAC/i-know-more-than-you-i-know-you.gif)

It's like a type cast in other languages, but it **doesn't perform any special checking or restructuring of data**.  
It's essentially a way to override the default behavior of the type system.

:::warning
It's usually a bad sign if you need a lot of type assertions.

Do _you_ really know better than TypeScript? 🤔  
Have you not forgotten a type check?  
Are you sure the variable can't be null or undefined?

**Remember, it doesn't actually do anything!**  
It's not really type casting, it's just telling TypeScript "trust me, it's an HTMLInputElement
and it's definitely not undefined".
:::

## Syntax

Type assertions can be done in two ways:

1. **Angle Bracket Syntax:**
   ```typescript
   let value: any = "Hello, TypeScript!"// -> ⛔
   let strLength: number = (<string>value).length
   ```

2. **As Syntax:**
   ```typescript
   let value: any = "Hello, TypeScript!"
   let strLength: number = (value as string).length
   ```

As we've seen with arrays, angle brackets are out of fashion.  
You _can_ use them (not in tsx), but should never mix both styles.

## When to Use Type Assertions

### 1. When Migrating from JavaScript

Type assertions are often used when migrating existing JavaScript code to TypeScript. In such cases, you might not have
complete type information, and type assertions can be a pragmatic approach to get started with static typing.

--> as you'll be doing this afternoon

### 2. When Working with DOM

When dealing with the DOM or other APIs that involve a degree of uncertainty, type assertions can be used to express
your confidence in the types.

--> as you'll be doing this afternoon

```typescript
const element = document.getElementById("myElement")
const elementId: string = element.id
// -> ⛔ 'element' is possibly 'null'

const element = document.getElementById("myElement")
// Use type assertion to tell TypeScript that element is not null
const elementId: string = (element as HTMLElement).id
```

```ts
const element = document.getElementById("myElement") as HTMLElement
const elementValue: string = element.value
// -> ⛔ Property 'value' does not exist on type 'HTMLElement'

const element = document.getElementById("myElement") as HTMLInputElement
const elementValue: string = element.value
// -> 👍

```

:::warning
As mentioned: using type assertions in this case indicates potentially bad code.

Do you really trust "myElement" do be present in HTML?  
Do this instead:

```ts
const element = document.getElementById("myElement")
if (element) {
    // Use type assertion to tell TypeScript that element is not null
    // Now that we **know** it isn't!
    const elementId: string = (element as HTMLElement).id
}
```
:::

## Quiz

[https://app.wooclap.com/YDFHJA](https://app.wooclap.com/YDFHJA?from=event-page)

## Exercise

Take
the [JS code from last week's assignment](https://gitlab.com/ikdoeict/public/vakken/full-stack-introductory-project/labo-web-apis/-/blob/master/app/public/js/script.js?ref_type=heads) 
and start refactoring to TS!

:::tip
Save it! You'll need it later! ⚠️
:::

