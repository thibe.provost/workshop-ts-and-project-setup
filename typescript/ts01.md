# Introduction to TypeScript

## Overview of TypeScript

- TypeScript is a superset of JavaScript
- Introduces static typing
- Compilation process: TypeScript to JavaScript
- Improved tooling and developer experience

:::tip 👀
Please read: [TypeScript for JavaScript Programmers](https://www.typescriptlang.org/docs/handbook/typescript-in-5-minutes.html)

And always use [the TypeScript handbook](https://www.typescriptlang.org/docs/handbook/intro.html) as source material
:::

## Recap

[JavaScript is a dynamic typed language](https://ikdoeict.gitlab.io/docenten/frontend/frontend_js-slides/02.javascript.html#/1)

**JavaScript is**... a little bit weird, a little bit unexpected...  
**JavaScript is**... freedom to do whatever you want, however you want to do it!  

![](https://media.tenor.com/hSdfEpVaBpUAAAAC/whitepeople-mud.gif)

...

**Strictly typed languages are**...  safe, predictable, reliable...

![](https://media.tenor.com/pbJM6SZnunwAAAAC/dancing-dance.gif)

...

::: tip Why though
So... why make JavaScript strictly typed 🥹

* bigger projects
* unknown dependencies
* colleagues 😱
* trying to understand the code you wrote last weekend
* develop libraries
* ...
:::

## Using TypeScript

* TypeScript is a super set of JavaScript **with** static typing
* TypeScript is a development tool
* TypeScript **cannot** run in the browser!
* TypeScript needs to be compiled to ECMAScript
* TypeScript was developed by Microsoft in 2012

::: info
It's highly unlikely you'll ever have to do these steps manually again.

See the second part of this workshop where we'll use vite in combination with TypeScript, ESLint, Prettier, etc.
:::

### Installing TypeScript

To install TypeScript globally, use the following npm command:

```bash
npm install -g typescript
```

### Compiling TypeScript to JavaScript

```bash
tsc filename.ts
```

## Basic Types

* TypeScript supports basic types like 'string', 'number', 'boolean', etc.
* Variables can be declared with a specific type

```ts
let message: string = "Hello, TypeScript!"
```

In this case we used an **explicit type annotation** to describe that the variable 'message' must be of type 'string'.

In most cases TypeScript is able to _infer_ the types for us, so we don't always have to write explicit type annotations.  
These are called **implicit types**

For the sake of clarity however, **let's always use explicit types for now**.

## Exercise

1. Start a new project
2. Create one file: example.ts

```ts
let text, index, success

text = "TypeScript"
index = "five"
success = !!text[index]

console.log(`Was getting the fifth character from the text a succes?\n${success}`)

```

3. Run `tsc example.ts`  
  Discuss the result  
  Tip: run `node example.js` to see your code in action
   
 
5. Run `tsc example.ts --strict`  
  Discuss the result

7. Run `tsc example.ts --strict --target esnext`  
  Discuss the result

9. Fix the error and prevent future errors!  
  Give every variable an explicit type (and assign an actual number to 'index').

![](https://media.tenor.com/1b-O-6aPde4AAAAC/yoda-amazed-i-am.gif)