# 4. Fix main.ts and make it all work

* Main.ts should only contain the init() function and import everything else
* Fix all TypeScript/ESLint/Prettier errors
* Ask help to improve this piece of code... it's... not good 🙈
  ```ts
  document.querySelector("#editTodoModal .modal-input").value =
    event.target.parentNode.parentNode.parentNode.textContent.trim()
  document.querySelector("#editTodoModal .modal-select").value =
    event.target.parentNode.parentNode.parentNode.firstElementChild.classList[1].trim()
  ```

::: danger
Don't forget to test your code in the browser!
:::
