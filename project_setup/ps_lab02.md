# 2. Refactor utils.ts

* Put the htmlToElement() function in utils.ts and export it
* Get rid of all TypeScript/ESLint/Prettier errors

To help you get started, here is the refactored and exported function
```ts
export function htmlToElement(html: string): HTMLElement {
  const template = document.createElement("template")
  html = html.trim() // Never return a text node of whitespace as the result
  template.innerHTML = html
  return template.content.firstChild as HTMLElement
}
```