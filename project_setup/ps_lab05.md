# 5. More

Do more than is required: pick at least one
* add unit tests with vitest
* add lint-staged
* add gitlab-ci
* experiment with sass

::: danger Assignment 📄
We want one final submission per team by the start of next week's lesson.  
Leave the URL of your repository in the shared Excel file.
:::