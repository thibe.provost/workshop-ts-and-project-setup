# 1. Directory structure

Create following files and directory structure

```
src/
  |-- img/              # Directory for images
  |-- styles/           # Directory for styles (SCSS, CSS, etc.)
        |-- style.scss  # Styles specific to your application
  |-- ts/               # Directory for TypeScript files
        |-- main.ts     # Main entry point for TypeScript
        |-- task-api.ts # TypeScript file for task-related API functionality
        |-- types.ts    # TypeScript file for type definitions
        |-- utils.ts    # TypeScript file for helper functions
  |-- vite-env.d.ts     # TypeScript declaration file for Vite environment
  
```