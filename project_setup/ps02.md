# Vite

:::tip 👀
See [front-end course: vite](https://ikdoeict.gitlab.io/docenten/frontend/frontend_js-slides/15.npm.html#/3)
:::

## Create a new project

```bash
pnpm create vite@latest
```

* Choose a project name

:::tip
Already created your directory??  
Type "." as project name, it will create the project in your **current** directory!
:::

* Select a framework: **Vanilla**  
  It means _plain_ JavaScript without a framework.
* Select a variant: TypeScript (obviously)
* Do what it says: pnpm install && pnpm run dev

:::info
We are now running our project using the vite dev server.

Try building the application! `pnpm run build`  
Which directory was created? Why? What's the difference?
:::

## Recreate the ToDo list

* replace index.html
  by [last week's version](https://gitlab.com/ikdoeict/public/vakken/full-stack-introductory-project/labo-web-apis/-/blob/master/app/public/home.html?ref_type=heads)
* replace js/script.js by src/main.ts (yes! .ts!)
  ```html
  <script type="module" src="js/script.js" defer></script> // [!code --]
  <script type="module" src="src/main.ts" defer></script> // [!code ++]
  ```
* replace the content of main.ts by the content
  of [last week's script.js](https://gitlab.com/ikdoeict/public/vakken/full-stack-introductory-project/labo-web-apis/-/blob/master/app/public/js/script.js?ref_type=heads)  
  **or your refactored script from this morning!**
* replace css/style.css by src/style.css
    ```html
  <link rel="stylesheet" href="css/style.css"> // [!code --]
  <link rel="stylesheet" href="src/style.css"> // [!code ++]
  ```
* replace the content of style.css by [last week's verion](https://gitlab.com/ikdoeict/public/vakken/full-stack-introductory-project/labo-web-apis/-/blob/master/app/public/css/style.css?ref_type=heads)
* add the required images, see lines 154-164 of styles.css
* Remove vite.svg, typescript.svg and counter.ts

You should now have a "working" app.

:::info
*A little word about this code
:::