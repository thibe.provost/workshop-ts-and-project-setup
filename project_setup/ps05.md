# StyleLint

It's the same... but for css

```bash
pnpm create stylelint
```

## Configuration

Lint the property sort order using the smaccs guidelines.

```bash
pnpm add -D stylelint-config-property-sort-order-smacss
```

Lint ook scss (sass) files

```bash
pnpm add -D stylelint-config-standard-scss 
```

in .stylelintrc
```json
{
  "extends": [
    "stylelint-config-standard-scss",
    "stylelint-config-property-sort-order-smacss"
  ]
}
```

Configureer je editor zodat die met stylelint werkt!  
Zowel voor css als voor scss: `{**/*,*}.{css,scss}`