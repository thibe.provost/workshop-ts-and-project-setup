# Getting started

## So you've 
* created a new Vite Project, configured TypeScript, ESLint, Prettier, StyleLint, Sass, Vitest...
* Added the HTML of last week's lab
* Added the css of last week's lab (and renamed it .scss)
* Got the project running, giving you a thousand errors?

Time to refactor!

## Assignment

::: danger Assignment 📄
We want one final submission per team by the start of next week's lesson.  
Leave the URL of your repository in the shared Excel file.
:::

::: info
The source code is by no means perfect.  
In fact, it's been passed down a few too many times over the years.

Feel free to improve where needed or open the discussion on teams!
:::









